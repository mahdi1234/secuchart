# Serverless P2P messengers

## Disambiguation

"P2P" is ill defined, thus we discuss its conflicting meanings separately:

* #pan_mesh_dtn
* #hardware
* #lan_messenger
* #wan_crowdsourcing
* cryptocurrency/blockchain

## Commonalities

* Many definitions call for client communication without mandated custom intermediaries.
* "Server" can also mean either a technicality (socket programming) and a privileged and manually administered node within the topology
* Keywords (imprecise buzzwords): decentralized, distributed.
